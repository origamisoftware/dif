# Data Ingestion Framework (DIF)

## Project Status:
The code in Master is 'complete' - that is everything there works as intended (barring any unfound bugs (of course))
and has unit tests with high code coverage. **However, overall, this is a WIP**  (work in progress). some of the code is stubbed or simply serves as a place holder. 

What currently works:
* adding files as part of a mutli file upload. (see the unit tests for examples of this)
* detecting when an upload is complete 

While there is not a .01 release yet, the code here is usable as is. 

What's next?
* add a lambda handler that uses this code. 
* deployment scripts for AWS 


## Overview

This framework makes it easy to process multi file uploads.
Designed to work in event driven environment such as AWS S3, 
this framework monitors multi file uploads and detects when the 
upload is complete. 

It works like this:

1) update a data file or a special file called `manifest.csv`. 

### manifest.csv
The `manifest.csv` file must contain a list of files in the upload. 
The specific contents of this file can be easily changed by modifying the code here, but a basic requirement is a list of files in the upload. 

The current code supports this CSV file format (but it can be easliy changed):


|                  |                                          |           |                |
| ---------------- | -----------------------------------------|-----------|----------------|
| manifest_id,     | three_3_jan28_19_any_unique_value_is_ok  |            |               |
| file_name,       | size,                                    | meta_data, | md5_checksum  |
| data_file1.txt,  | 11100,                                   | comment,   | 6000          |
| data_file2.txt,  | 121100,                                  | comment,   | 34211         |
| data_file3.txt,  | 1231100,                                 | comment,   | 94211         |

Note: manifest.csv does not contain a reference to itself. 

See `modules.test.data` for examnples of actual manifest.csv files used for testing. 

2a) When the manifest file is uploaded, it is contents is parsed and placed into a database. Records are created for each expected file and a record is created for the manifest. 
The table that holds upload data is called Upload _
The table that holds file data is called simply file. 
see `database_model.py`

2b) If a file called anything other than `manifest.csv` shows up, a check for an existing  file record is made. If there is not one present, a file record is created. 

2c)  If a file called anything other than `manifest.csv` shows up, a check for an existing  file record is made. If there is one present there should be a corresponding file record already created. Find it. It is not present, that’s an error. 

3) Evidently  `Upload.is_complete` will be true.  When this happens, generate an event (message) _

3a) A background job (not currently in this code base) should be run to see if there are any` Upload.is_complete  = False` with a last updated time sufficiently in the past  to indicate somethings broken. When detected trigger an alert.  

## What's here (and why)
├── README.md  

this file!


├── bitbucket-pipelines.yml

bitbucket continous intergration configuration. 


├── doc

project documentation located (mostly) here.

│   └── project_guide.md
	
	overall documentation (WIP) 

├── modules

│   ├── aws

│   │   └── lambda_handler.py

		lambda code goes here. 

│   ├── config
	
	 	this module contains code that provides dynamic enviroment configuration

│   │   ├── aws_settings.py

│   │   ├── local_postgres_settings.py

│   │   ├── test_in_memory_settings_sqlite.py

│   │   └── test_settings_sqlite.py


│   ├── data_access_objects

│   │   └── database_model.py

		this module contains the ORM classes (using sqlalchemy)

│   ├── ingest

│   │   └── data_ingestion.py

		this module contains the code that processes incoming files.

│   ├── model

│   │   └── manifest.py

		the model package contains code that represent data elements in the application. 

			manifest.py contains code for reading manifest files into a model of that file. 

│   ├── test

		test code is located in this module 

│   │   ├── Test_Setup_Instructions.md

			instructions for how to run tests successfully. 

│   │   ├── aws_lambda_mock.py

			this code allows tests to pretend they are running in the actual AWS environment.

│   │   ├── data

			this directory contains test data and code that allows easy access to that data. 

│   │   │   ├── constants.py

│   │   │   ├── lambda-context.json

│   │   │   ├── lambda-event.json

│   │   │   ├── test_data_accessor.py

│   │   │   ├── test_data_set_1_negative

│   │   │   │

│   │   │   ├── test_data_set_2_negative

│   │   │   ├── test_data_set_3_negative

│   │   │   ├── test_data_set_4_negative

│   │   │   ├── test_data_set_one

│   │   │   ├── test_data_set_three

│   │   │   └── test_data_set_two

│   │   ├── test_config.py

│   │   ├── test_data_ingestion.py

│   │   ├── test_database.py

│   │   └── test_lambda_handlers.py

│   └── utils
        
        shared code 
    
│       ├── file_utils.py

│       └── logging_configuration.py

    pip install file 
├── requirements.txt

    AWS SAM config file - used for making deployable Lambda apps (currently a place holder)
└── template.yml


If you are interested in helping with this project please contact me: smarks@origamisoftware.com 

