# How to work with the DIF project

## setup 

### install the AWS SAM command line app 
https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install-mac.html
IMPORTANT NOTES:
- using brew did not work for me. 
- using pip did:

<code>
pip install --user aws-sam-cli
</code>

## getting started 
  - opitional - use a virtual environment. 
  - pip install --upgrade pip
  - pip install -r requirements.txt


## running tests

`python -m unittest discover .`

## making a lamda package
`tbd`

