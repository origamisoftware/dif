import csv
import os.path

from modules.utils.logging_configuration import get_logger

logger = get_logger(__file__)

"""A ManifestEntry instance holds a line item in a manifest file. 
One or more Manifest_Entry objects point to a file that must be present in order 
for the data delivery to be complete. 
"""


class ManifestEntry:
    # these index values map directory to expected columns in the CSV file.
    FILE_NAME_INDEX = 0
    SIZE_INDEX = 1
    META_DATA_INDEX = 2
    MD5_CHECKSUM_INDEX = 3

    '''given a csv row map that data to a manifest entry.
    The manifest parameter is maps to the actual manifest file '''

    def __init__(self, row, base_url):
        if len(row) != 0:
            self.file_name = row[self.FILE_NAME_INDEX].strip()
            self.size = row[self.SIZE_INDEX].strip()
            self.meta_data = row[self.META_DATA_INDEX].strip()
            self.md5_checksum = row[self.MD5_CHECKSUM_INDEX].strip()
            self.manifest_identifier = None
            self.url = base_url + "/" + self.file_name

    def __repr__(self):
        return '%s %s %s %s' % (
            self.file_name,
            self.size,
            self.meta_data,
            self.md5_checksum)

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items())
        )


""" The ManifestFile class models the actual manifest.csv file
"""


class ManifestFile:
    """ bucket name is the folder or directory the manifest resides in
    url is the whole path the the bucket
    contents is the entire contents of the file - the raw csv data"""

    def __init__(self, group_identifier, url, contents):
        self.group_identifier = group_identifier
        self.url = url
        self.contents = contents
        self.manifest_entries = []
        self.manifest_identifier = self._parse(self.contents)
        file_name = os.path.basename(url)
        self.file_name = file_name

    def __repr__(self):
        return '%s %s %s' % (
            self.group_identifier,
            self.url, self.contents)

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items())
        )

    def _parse(self, contents):
        """ parses manifest contents which should be in the specific csv format.
               Returns the natural (user generated) manifest id.
               (Don't confuse this ID with the db generated primary key) """

        reader = csv.reader(contents.split('\n'), delimiter=',')

        # read the manifest id
        first_row = next(reader, None)
        self.manifest_identifier = first_row[1].strip()
        second_row_column_headers = next(reader, None)
        logger.debug("Skipping column headers: " + str(second_row_column_headers))
        # read everything else into an array
        for row in reader:
            manifest_entry = ManifestEntry(row, self.group_identifier)
            self.manifest_entries.append(manifest_entry)

        return self.manifest_identifier
