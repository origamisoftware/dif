""" this file contains constants and other data test for the project"""

NUMBER_OF_ENTRIES = '2'
BUCKET_NAME = "bucket_name"
MANIFEST_ID = '1'
FILE_NAME = 'fileName'
FILE_SIZE = '100'
META_DATA = "Something interesting hopefully"
CHECKSUM = '10010'
SECOND_FILE_NAME = 'path/fileName'
SECOND_SIZE = '2000'
SECOND_META_DATA = 'more meta data'
SECOND_CHECKSUM = '101010'
URL = "S3://blah/blah/third"
THIRD_FILE = 'path / owl_nested_file_name'
THIRD_SIZE = '200`'
THIRD_META_DATA = 'even more meta data'
THIRD_CHECKSUM = '1310'

S3_FILE_NAME_1 = 'file_name_one'
S3_FILE_NAME_2 = 'path/file_name_two'
S3_FILE_NAME_3 = 'path/path/file_name_three'
S3_FILE_NAME_4 = 'path/path/path/file_name_four'

CSV_DATA = "manifest_id, " + MANIFEST_ID + """ ,file_name, size, meta_data, md5_checksum \n""" \
           + FILE_NAME \
           + "," \
           + FILE_SIZE \
           + "," \
           + META_DATA \
           + "," \
           + CHECKSUM \
           + "\n" \
           + SECOND_FILE_NAME \
           + "," \
           + SECOND_SIZE \
           + "," \
           + SECOND_META_DATA \
           + "," \
           + SECOND_CHECKSUM \
           + "," \
           + THIRD_FILE \
           + "," \
           + THIRD_SIZE \
           + "," \
           + THIRD_META_DATA \
           + "," \
           + THIRD_CHECKSUM
