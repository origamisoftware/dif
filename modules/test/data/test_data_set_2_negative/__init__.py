import csv
import os

from modules.utils.file_utils import get_file_contents


def get_location():
    return os.path.dirname(__file__)


def get_manifest_file():
    return "file://" + get_location() + "/manifest.csv"


def get_all_data_files():
    return_value = []
    file_list = os.listdir(get_location())

    for file in file_list:
        if file.endswith(".txt"):
            return_value.append("file://" + get_location() + file)

    return return_value


def get_manifest_identifier():
    with open(get_location() + "/manifest.csv") as f:
        contents = f.read()

    contents = contents.strip()
    reader = csv.reader(contents.split('\n'), delimiter=',')
    first_row = next(reader, None)
    return first_row[1].strip()


def get_manifest_contents():
    return get_file_contents(get_location() + "/manifest.csv")
