import csv
import os
from enum import Enum

from modules.utils.file_utils import get_file_contents

""" This a test data helper class. Hopefully the method names
are self-documenting
"""


def get_location():
    return os.path.dirname(__file__)


def get_manifest_file(data_set_enum):
    return "file://" + data_set_enum.value + "/manifest.csv"


def get_all_data_files(data_set_enum):
    return_value = []
    file_list = os.listdir(data_set_enum.value)

    for file in file_list:
        if file.endswith(".txt"):
            return_value.append("file://" + data_set_enum.value + "/" + file)

    return return_value


def get_manifest_identifier(data_set_enum):
    with open(data_set_enum.value + "/manifest.csv") as f:
        contents = f.read()

    contents = contents.strip()
    reader = csv.reader(contents.split('\n'), delimiter=',')
    first_row = next(reader, None)
    return first_row[1].strip()


def get_manifest_contents(data_set_enum):
    return get_file_contents(data_set_enum.value + "/manifest.csv")


class DataSets(Enum):
    DATA_SET_ONE = get_location() + "/test_data_set_one"
    DATA_SET_TWO = get_location() + "/test_data_set_two"
    DATA_SET_THREE = get_location() + "/test_data_set_three"
    DATA_SET_FOUR = get_location() + "/test_data_set_four"
    DATA_SET_ONE_NEGATIVE = get_location() + "/test_data_set_1_negative"
    DATA_SET_TWO_NEGATIVE = get_location() + "/test_data_set_2_negative"
    DATA_SET_THREE_NEGATIVE = get_location() + "/test_data_set_3_negative"
    DATA_SET_FOUR_NEGATIVE = get_location() + "/test_data_set_4_negative"
