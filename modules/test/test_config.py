import unittest


class TestConfigurations(unittest.TestCase):

    def test_test_settings(self):
        from modules.config.test_settings_sqlite import db_url as test_db_url
        self.assertIsNotNone(test_db_url)

    def test_aws_settings(self):
        from modules.config.aws_settings import db_url as aws_db_url
        self.assertIsNotNone(aws_db_url)

    def test_in_memory_settings(self):
        from modules.config.test_in_memory_settings_sqlite import db_url as in_memory_db_url
        self.assertIsNotNone(in_memory_db_url)


if __name__ == '__main__':
    unittest.main()
