import unittest

import os
import os.path
from urllib.parse import urlparse

#if os.environ['DB_TYPE'] is None:
from modules.config.test_in_memory_settings_sqlite import db_url
#elif os.environ['DB_TYPE'] is 'Postgres':
#    from modules.config.local_postgres_settings import db_url
#else:
#    raise ValueError( os.environ['DB_TYPE'] + " is an unsupported 'DB_TYPE' type") 

from modules.data_access_objects.database_model import File
from modules.data_access_objects.database_model import Upload
from modules.data_access_objects.database_model import data_access
from modules.ingest.data_ingestion import get_group_identifier_record
from modules.model import manifest
from modules.test.data.constants import BUCKET_NAME, URL, CSV_DATA
from modules.utils.logging_configuration import get_logger

logger = get_logger(__file__)


class DatabaseTesting(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        data_access.conn_string = db_url
        logger.debug('data_access.conn_string is: ' + data_access.conn_string)
        data_access.connect()
        data_access.session = data_access.Session()
        prep_db()
        data_access.session.close()

    def setUp(self):
        data_access.session = data_access.Session()

    def tearDown(self):
        logger.debug('data_access.session.rollback() called')
        data_access.session.rollback()
        data_access.session.close()
        logger.debug('data_access.session.closed() called')

    @classmethod
    def tearDownClass(cls):
        data_access.drop_all()

    def test_read(self):
        query = data_access.session.query(Upload)
        query_all = query.all()
        self.assertEqual(len(query_all), 1)


def prep_db():
    manifest_file = manifest.ManifestFile(BUCKET_NAME, URL, CSV_DATA)
    parsed_url = urlparse(URL)
    file_name = os.path.basename(parsed_url.path)
    group_identifier_record = get_group_identifier_record(file_name, parsed_url)
    upload = Upload(manifest_file, group_identifier_record.id)
    data_access.update(upload)
    upload = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_file.manifest_identifier).first()
    files = manifest_file.manifest_entries
    for entry in files:
        kwargs = dict(file_name=entry.file_name, is_present=True, file_url=entry.url, md5_checksum=entry.md5_checksum,
                      meta_data=entry.meta_data,
                      size=entry.size, group_identifier_fk=upload.group_identifier_fk, manifest_fk=upload.id)
        file = File(**kwargs)
        data_access.update(file)


if __name__ == '__main__':
    unittest.main()
