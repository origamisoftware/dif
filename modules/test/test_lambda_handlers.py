import unittest

from modules.utils.logging_configuration import get_logger

logger = get_logger(__file__)


class LambdaHandlerTests(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        logger.debug("setUpClass needs completion")

    def setUp(self):
        logger.debug("setUp needs completion")

    def tearDown(self):
        logger.debug("tearDown needs completion")

    @classmethod
    def tearDownClass(cls):
        logger.debug("tearDownClass needs completion")


if __name__ == '__main__':
    unittest.main()
