import unittest

from sqlalchemy.exc import IntegrityError

import modules.ingest.data_ingestion as data_ingestion
import modules.test.data.test_data_accessor as test_data
from modules.config.test_settings_sqlite import db_url
from modules.data_access_objects.database_model import Upload
from modules.data_access_objects.database_model import data_access
from modules.model.manifest import ManifestFile
from modules.utils.logging_configuration import get_logger

logger = get_logger(__file__)


class DataIngestionTesting(unittest.TestCase):

    def _verify_state(self, path_to_manifest_file, use_this_test_data, data_set_enum):
        manifest_file = ManifestFile(path_to_manifest_file, path_to_manifest_file,
                                     use_this_test_data.get_manifest_contents(data_set_enum))
        entries = manifest_file.manifest_entries
        upload = data_access.session.query(Upload).filter_by(
            manifest_identifier=manifest_file.manifest_identifier).first()
        files = upload.files
        self.assertEqual(len(entries), len(files), "files found")

        found = False
        if set([entry.file_name for entry in entries]) & set([file.file_name for file in files]):
            found = True
        self.assertTrue(found, "file found")

    def setUp(self):
        self._init_db()
        data_access.drop_all()
        self._init_db()

    def _init_db(self):
        data_access.conn_string = db_url
        data_access.connect()
        data_access.session = data_access.Session()

    def tearDown(self):
        data_access.drop_all()

    def test_add_manifest_file_local(self):
        path_to_manifest_file = test_data.get_manifest_file(test_data.DataSets.DATA_SET_ONE)
        data_ingestion.add_a_file(path_to_manifest_file)
        self._verify_state(path_to_manifest_file, test_data, test_data.DataSets.DATA_SET_ONE)

    def test_add_manifest_from_s3(self):
        path_to_manifest_file = test_data.get_manifest_file(test_data.DataSets.DATA_SET_TWO)
        data_ingestion.add_a_file(path_to_manifest_file)
        self._verify_state(path_to_manifest_file, test_data, test_data.DataSets.DATA_SET_TWO)

    def test_add_data_file_from_local(self):
        complete_path = test_data.get_all_data_files(test_data.DataSets.DATA_SET_ONE)[0]
        data_ingestion.add_a_file(complete_path)

    def test_add_all_files(self):
        files = test_data.get_all_data_files(test_data.DataSets.DATA_SET_TWO)
        for f in files:
            data_ingestion.add_a_file(f)

    def test_process_file_before_manifest_available(self):
        path_to_manifest = test_data.get_manifest_file(test_data.DataSets.DATA_SET_ONE)
        files = test_data.get_all_data_files(test_data.DataSets.DATA_SET_ONE)

        # add files before manifest
        for f in files:
            data_ingestion.add_a_file(f)

        # now add manifest
        data_ingestion.add_a_file(path_to_manifest)

    def test_process_file_after_manifest_upload(self):
        manifest_file = test_data.get_manifest_file(test_data.DataSets.DATA_SET_TWO)
        # add manifest first
        data_ingestion.add_a_file(manifest_file)

        # then add files
        files = test_data.get_all_data_files(test_data.DataSets.DATA_SET_TWO)
        for f in files:
            data_ingestion.add_a_file(f)

    def test_process_files_manifest_in_the_middle(self):
        files = test_data.get_all_data_files(test_data.DataSets.DATA_SET_ONE)
        manifest_identifier = test_data.get_manifest_identifier(test_data.DataSets.DATA_SET_ONE)

        data_ingestion.add_a_file(files[0])
        data_ingestion.add_a_file(files[1])

        upload = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier).first()
        self.assertIsNone(upload)

        # now add manifest
        path_to_manifest = test_data.get_manifest_file(test_data.DataSets.DATA_SET_ONE)
        data_ingestion.add_a_file(path_to_manifest)

        upload = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier).first()
        self.assertFalse(upload.is_complete)

        # add some more files
        data_ingestion.add_a_file(files[2])
        data_ingestion.add_a_file(files[3])

        # the last file completes the upload, check that upload is complete.
        upload = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier).first()
        self.assertTrue(upload.is_complete, "upload should be marked complete")

    def test_a_bunch_of_uploads_at_once(self):

        files_one = test_data.get_all_data_files(test_data.DataSets.DATA_SET_ONE)
        manifest_identifier_one = test_data.get_manifest_identifier(test_data.DataSets.DATA_SET_ONE)
        upload1 = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier_one).first()
        self.assertIsNone(upload1, "Upload 1 should not be present yet")

        data_ingestion.add_a_file(files_one[0])
        data_ingestion.add_a_file(files_one[1])

        files_two = test_data.get_all_data_files(test_data.DataSets.DATA_SET_TWO)

        data_ingestion.add_a_file(files_two[0])
        data_ingestion.add_a_file(files_two[1])
        data_ingestion.add_a_file(test_data.get_manifest_file(test_data.DataSets.DATA_SET_TWO))
        manifest_identifier_two = test_data.get_manifest_identifier(test_data.DataSets.DATA_SET_TWO)
        upload2 = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier_two).first()
        self.assertFalse(upload2.is_complete, "upload 2 should be incomplete")

        files_three = test_data.get_all_data_files(test_data.DataSets.DATA_SET_THREE)
        manifest_identifier_three = test_data.get_manifest_identifier(test_data.DataSets.DATA_SET_THREE)
        upload3 = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier_three).first()
        self.assertIsNone(upload3, "upload 3 should be not be created yet")

        data_ingestion.add_a_file(files_three[0])

        data_ingestion.add_a_file(files_one[2])

        data_ingestion.add_a_file(files_three[1])

        data_ingestion.add_a_file(files_one[3])

        data_ingestion.add_a_file(files_three[2])

        data_ingestion.add_a_file(test_data.get_manifest_file(test_data.DataSets.DATA_SET_ONE))
        upload1 = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier_one).first()
        self.assertTrue(upload1.is_complete, "upload 1 should be complete")

        data_ingestion.add_a_file(test_data.get_manifest_file(test_data.DataSets.DATA_SET_THREE))
        upload3 = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier_three).first()
        self.assertFalse(upload3.is_complete, "upload 3 should be incomplete")

        data_ingestion.add_a_file(files_three[3])
        upload3 = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier_three).first()
        self.assertTrue(upload3.is_complete, "upload 3 should be complete")

        upload2 = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier_two).first()
        self.assertFalse(upload2.is_complete, "upload 2 should be incomplete")

        data_ingestion.add_a_file(files_two[2])
        data_ingestion.add_a_file(files_two[3])
        upload2 = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier_two).first()
        self.assertTrue(upload2.is_complete, "upload 2 should be complete")

    def test_a_duplicate_manifest_id_negative(self):
        files_one = test_data.get_all_data_files(test_data.DataSets.DATA_SET_ONE)
        data_ingestion.add_a_file(files_one[2])
        data_ingestion.add_a_file(files_one[1])
        manifest_identifier_one = test_data.get_manifest_identifier(test_data.DataSets.DATA_SET_ONE)
        upload1 = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier_one).first()
        self.assertIsNone(upload1, "Upload 1 should not be present yet")
        data_ingestion.add_a_file(test_data.get_manifest_file(test_data.DataSets.DATA_SET_ONE))
        upload1 = data_access.session.query(Upload).filter_by(manifest_identifier=manifest_identifier_one).first()
        self.assertFalse(upload1.is_complete, "upload 1 should be incomplete")

        self.assertRaises(IntegrityError, data_ingestion.add_a_file,
                          test_data.get_manifest_file(test_data.DataSets.DATA_SET_ONE))

    def test_invalid_size_data(self):
        if 'sqlite' not in db_url:
            self.assertRaises(IntegrityError, data_ingestion.add_a_file,
                              test_data.get_manifest_file(test_data.DataSets.DATA_SET_TWO_NEGATIVE))

    def test_add_a_duplicate_file_negative(self):
        if 'sqlite' not in db_url:
            files_one = test_data.get_all_data_files(test_data.DataSets.DATA_SET_ONE)

            data_ingestion.add_a_file(files_one[2])
            data_ingestion.add_a_file(files_one[1])

            self.assertRaises(IntegrityError, data_ingestion.add_a_file, files_one[1])

    def test_add_a_invalid_manifest_file_negative(self):
        if 'sqlite' not in db_url:
            self.assertRaises(IntegrityError,
                              data_ingestion.add_a_file(
                                  test_data.get_manifest_file(test_data.DataSets.DATA_SET_TWO_NEGATIVE)))

    if __name__ == '__main__':
        unittest.main()
