# Important Test Setup Instructions 
 
## Required Environment Variables 

Running tests successfully requires the presence of the environment variables. 

key - <code>DB_TYPE</code>

with a value of:

<code>sqlite</code>

<code>postgres</code>


See modules/test/test_database.py:7

for how this environment variable is used. 

Defining an environment variables so they are available in the python code itself seems to be plateform dependant. [TODO https://bitbucket.org/seres_therapeutics/dif/issues/3/update-to-modules-test] 



NOTE: add values per configuration. 

ALSO NOTE: for Bitbucket, environment variables are added in the bitbucket console UI

https://bitbucket.org/seres_therapeutics/dif/admin/addon/admin/pipelines/repository-variables
 
