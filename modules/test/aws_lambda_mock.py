import json
import logging
import os

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

LAMBDA_EVENT_TEMPLATE = './data/lambda-event.json'
LAMBDA_CONTEXT_TEMPLATE = './data/lambda-context.json'


# The event data read from template

class LambdaEvent:

    def __init__(self):
        self.event = None

    def load_event(self):
        if os.path.exists(LAMBDA_EVENT_TEMPLATE):
            try:
                logger.info('Reading lambda-event-json')
                init_event_data = json.loads(open(LAMBDA_EVENT_TEMPLATE).read())
                self.event = init_event_data
            except Exception as e:
                logger.error(e)

    def set_field(self, key, value):
        if self.event:
            if key in self.event:
                self.event[key] = value


class LambdaContext:
    def __init__(self):
        self.context = None
        self.event = None

    def load_event(self):
        if os.path.exists(LAMBDA_EVENT_TEMPLATE):
            try:
                logger.info('Reading lambda-event-json')
                init_context_data = json.loads(open(LAMBDA_CONTEXT_TEMPLATE).read())
                self.context = init_context_data
            except Exception as e:
                logger.error(e)

    def set_field(self, key, value):
        if self.event:
            if key in self.event:
                self.event[key] = value
