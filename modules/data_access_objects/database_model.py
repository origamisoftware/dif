from datetime import datetime
from sqlalchemy import Column, Integer, String, DateTime, ForeignKey, Boolean, UniqueConstraint
from sqlalchemy import create_engine
from sqlalchemy.orm import relationship, backref, sessionmaker

from modules.data_access_objects import Base
from modules.utils.logging_configuration import get_logger

''' This class represents a single file that is associated with a specific manifest
'''

''' This class contains all the information to track and manage an upload. 
it contains the manifest data as well as whether or not the the upload is complete
'''

logger = get_logger(__file__)

"""The DataAccessLayer is used to access the configured database. """


class DataAccessLayer:

    def __init__(self):
        self.engine = None
        self.session = None
        self.conn_string = None
        self.Session = None

    def connect(self):
        self.engine = create_engine(self.conn_string)
        Base.metadata.create_all(self.engine)
        self.Session = sessionmaker(bind=self.engine)
        logger.debug("DataAccessLayer.connect executed")

    def drop_all(self):
        """Delete all the tables - be careful"""
        Base.metadata.drop_all(self.engine)

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items())
        )

    @staticmethod
    def update(update_or_add_me):
        """Used to update or create new records"""
        data_access.session.add(update_or_add_me)
        logger.debug("about to commit: " + str(update_or_add_me))
        data_access.session.commit()


""" Clients use this data_access instance to interact with the db"""
data_access = DataAccessLayer()

""" The Upload class keeps track of the entire set of files being uploaded. 
    Is the ORM class that maps to a manifest file. 
    It contains the complete contents of the manifest file as a a big string of text.
    It contains the list of Files associated with the manifest (excluding the manifest file
    itself.  
    For every upload there should be one upload record which will have a unique group_identifier_fk 
"""


class Upload(Base):
    __tablename__ = 'upload'

    # primary key
    id = Column(Integer, primary_key=True)

    # record keeping
    created_on = Column(DateTime(), default=datetime.now, nullable=False)
    updated_on = Column(DateTime(), default=datetime.now, onupdate=datetime.now)

    # contents of the entire manifest file
    manifest_contents = Column(String, nullable=False)

    # are all the files in the manifest also uploaded? (i.e. ready for processing)
    is_complete = Column(Boolean)

    # unique id for all the files in an upload -
    # it is the actual location of the files i.e. S3 bucket, or parent directory
    group_identifier_fk = Column(Integer(), ForeignKey('group.id'), nullable=False)

    # the 'user friendly' manifest identification (not the PK)
    manifest_identifier = Column(String, unique=True, nullable=False)

    # the list of files in the manifest
    files = relationship("File", backref=backref('files', order_by=id))

    # todo
    has_been_batched = False

    def __init__(self, manifest, group_identifier_id):
        self.manifest_contents = manifest.contents
        self.is_complete = False
        self.group_identifier_fk = group_identifier_id
        self.manifest_identifier = manifest.manifest_identifier

    def __repr__(self) -> str:
        return super().__repr__()

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items())
        )


""" This class represents a single file in a multi file upload 
    It is the ORM class for manifest entry objects. 
    A manifest contains 1 or more manifest entries. 
    A manifest entry object represents a single file that is part of the upload. 
    The list of manifest entries represents all the files in an upload 
    save for the actual manifest file itself - which is not listed as manifest entry
"""


class File(Base):
    __tablename__ = 'file'

    # primary key
    id = Column(Integer, primary_key=True)

    # record keeping
    created_on = Column(DateTime(), default=datetime.now, nullable=False)
    updated_on = Column(DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)

    # the file name - not the path or anything else.
    file_name = Column(String(255), index=True, nullable=False)

    # the physical location of the file.
    file_url = Column(String(255), index=True, unique=True, nullable=False)

    # the size of the file (read from manifest)
    size = Column(Integer(), nullable=True)

    # the md5_checksum of the file (read from manifest)
    md5_checksum = Column(String, nullable=True)

    # any additional data associated with file (read from manifest)
    meta_data = Column(String, nullable=True)

    # has this file be uploaded / digested
    is_present = Column(Boolean)

    # unique id for all the files in an upload -
    # it is the actual location of the files i.e. S3 bucket, or parent directory
    group_identifier_fk = Column(Integer(), ForeignKey('group.id'), nullable=False)

    # db generated key
    manifest_fk = Column(Integer(), ForeignKey('upload.id'), nullable=True)

    # the upload this file is associated with.
    upload = relationship("Upload", backref=backref('upload', order_by=id))

    # is this the data file or the manifest
    is_data_file = Column(Boolean)

    UniqueConstraint('file_name', 'group_identifier_fk')

    def __init__(self, **kwargs):

        self.file_name = kwargs['file_name']
        self.is_present = kwargs['is_present']
        self.size = kwargs['size']
        self.file_url = kwargs['file_url']
        self.md5_checksum = kwargs['md5_checksum']
        self.meta_data = kwargs['meta_data']
        self.group_identifier_fk = kwargs['group_identifier_fk']
        self.manifest_fk = kwargs['manifest_fk']
        if self.file_name is 'manifest.csv':
            self.is_data_file = False
        else:
            self.is_data_file = True

    def __repr__(self):
        return self.file_name

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items())
        )


""" This ORM class is used to join Files with there corresponding Upload record. 
    It normalizes that location data for a file and an upload. 
    The location (the URL to the upload and its files) has to be unique. 
    So we treat the location as a natural key and call it a GroupIdentifier
"""


class GroupIdentifier(Base):
    __tablename__ = 'group'

    # primary key
    id = Column(Integer, primary_key=True)

    # record keeping
    created_on = Column(DateTime(), default=datetime.now, nullable=False)
    updated_on = Column(DateTime(), default=datetime.now, onupdate=datetime.now, nullable=False)

    # location (the path to the parent directory / bucket)
    location = Column(String, unique=True, nullable=False)

    # db generated key for the manifest
    manifest_fk = Column(Integer(), ForeignKey('upload.id'), nullable=True)

    # the list of files in the manifest
    files = relationship("File", backref=backref('file', order_by=id))

    def __init__(self, location):
        self.location = location
        logger.debug("group object created: " + self.__str__())

    def __str__(self):
        return '%s(%s)' % (
            type(self).__name__,
            ', '.join('%s=%s' % item for item in vars(self).items())
        )
