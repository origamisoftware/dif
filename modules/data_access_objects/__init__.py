from sqlalchemy.ext.declarative import declarative_base

"""This module provides ORM classes to be used with sqlalchemy. 
   Each table in the db is modeled here.
"""

Base = declarative_base()
