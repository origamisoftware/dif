""" This configuration is used to run code using in file based sqlite db - this might be helpful for debugging
"""

from modules.utils.logging_configuration import get_logger

logger = get_logger(__file__)

db_url = "postgresql://localhost:5432/postgres"
logger.info("postgres test db is here: " + db_url)
