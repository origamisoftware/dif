""" This configuration is used to run code using in file based sqlite db - this might be helpful for debugging
"""

import os
import tempfile

from modules.utils.logging_configuration import get_logger

logger = get_logger(__file__)

''' this configuration creates sqlite db using a file in '''
temp_dir_path = tempfile.gettempdir()
temp_db_file_name = "test.db"
temp_db = temp_dir_path + "/" + "test.db"

# if db does not exist, create an empty file for it.
if not os.path.exists(temp_db):
    fh = open(temp_db, "w")
    fh.close()
    logger.debug("sqlite db did not exist, creating an empty db")

db_url = 'sqlite:///' + temp_db
logger.warn("Using a file based sqlite db is not recommended for testing as db constraints aren't enforced.")
logger.info("sqlite test db is here: " + temp_db)
