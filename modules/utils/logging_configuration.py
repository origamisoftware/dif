import logging
import os

FORMAT = '%(levelname)s * %(asctime)-15s * %(lineno)d * %(filename)s * %(funcName)-8s : %(message)s'
logging.basicConfig(format=FORMAT)
formatter = logging.Formatter(FORMAT)

logging_level = os.environ.get('LOGGING_LEVEL', 'DEBUG')
logging.basicConfig(level=logging_level)


def get_logger(name):
    """Returns the logger for the given name"""
    logger = logging.getLogger(name)
    logger.setLevel(os.environ.get('LOGGING_LEVEL', 'DEBUG'))
    return logger
