""" Common code goes here! """


def get_file_contents(path):
    """This method returns the contents of the file pointed to by the path parameter
    """

    with open(path) as f:
        contents = f.read()

    return contents.strip()
