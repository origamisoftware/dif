import os.path
from sqlalchemy import and_
from urllib.parse import urlparse

from modules.data_access_objects.database_model import File
from modules.data_access_objects.database_model import GroupIdentifier
from modules.data_access_objects.database_model import Upload
from modules.data_access_objects.database_model import data_access
from modules.model.manifest import ManifestFile
from modules.utils.file_utils import get_file_contents
from modules.utils.logging_configuration import get_logger

logger = get_logger(__file__)

""" This class contains methods for adding file data to the db
    It has a single public entry point for doing that:
        add_a_file(url):
"""


def add_a_file(url):
    """ Process an incoming file. Path needs to be a valid URI.
        e.g. file:///<path>
             s3:///<path>
    """
    # the complete URL we want the file name and the path so we use urlparse
    parsed_url = urlparse(url)

    file_name = os.path.basename(parsed_url.path)

    group_identifier_record = get_group_identifier_record(file_name, parsed_url)

    # add the file to db based on of type (either manifest or data file)
    if file_name.endswith('manifest.csv'):
        _ingest_manifest(parsed_url, group_identifier_record)
    else:
        _process_file(file_name, group_identifier_record)

    # check to see if the current file completed the upload and if so mark upload complete.
    upload = data_access.session.query(Upload).filter_by(group_identifier_fk=group_identifier_record.id).first()
    if upload:
        all_files_present = True
        files = upload.files
        for file in files:
            if file.is_present is False:
                all_files_present = False

        if all_files_present:
            upload.is_complete = True
            data_access.update(upload)


def _does_this_file_complete_the_upload(file):
    """Returns true if the provided file completes the upload and false otherwise. """

    if file.manifest_fk is None:
        return False

    upload = data_access.session.query(Upload).filter_by(group_identifier_fk=file.group_identifier_fk).first()
    for file in upload.files:
        if file.is_present is False:
            return False

    return True


def get_group_identifier_record(file_name, parsed_url):
    """ Given a url return the corresponding group_identifier_record creating one if it
        doesn't already exist.
    """
    # Group_identifier is the URL without the file name. We call that the location.
    group_identifier = parsed_url.scheme + "://" + parsed_url.path[:-len(file_name)]

    # determine if the group_identifier_record exists already (have we already seen this location?)
    group_identifier_record = data_access.session.query(GroupIdentifier).filter_by(location=group_identifier).first()

    # we have not, so create a new group_identifier_record.
    if group_identifier_record is None:
        group_identifier_record = GroupIdentifier(group_identifier)
        data_access.update(group_identifier_record)

    return group_identifier_record


def _ingest_manifest(parsed_url, group_identifier_record):
    """ This method parses the manifest file and stores its' data in the database.
    """
    url = parsed_url.geturl()

    # if the scheme is local we can directly access the manifest file and parse it.
    if parsed_url.scheme == 'file':
        _process_manifest(parsed_url.path, parsed_url, group_identifier_record)
    # if the manifest is in s3 bucket, we first have to download the file from S3 and then parse it.
    elif parsed_url.scheme == 's3':
        local_manifest_file = _get_manifest_from_s3(url)
        _process_manifest(local_manifest_file, parsed_url, group_identifier_record)
    # currently only file and s3 schemes are supported.
    else:
        raise Exception("Unsupported scheme: " + parsed_url.scheme)


def _process_file(file_name, group_identifier_record):
    """ This method puts a  file record (not the manifest) into the db for the given file.
          group_identifier_record.location + "/" file_name
        is really the URL for the particular file.
        However, using the group_identifier_record allows us to normalize the
        location of the file and add a reference to it to it's corresponding upload object
        (if that upload object has already been created.)
    """
    file_url = group_identifier_record.location + file_name
    group_identifier_fk = group_identifier_record.id

    # if the manifest  has be ingested, get its corresponding upload record.
    upload = data_access.session.query(Upload).filter_by(group_identifier_fk=group_identifier_fk).first()

    if upload is None:  # we haven't see the manifest file yet.

        # the None values will be replaced with real values once manifest file is ingested
        # When the manifest file is processed these values will be available
        # and file record being created here will be updated.
        kwargs = dict(file_name=file_name, is_present=True, file_url=file_url, md5_checksum=None, meta_data=None,
                      size=None, group_identifier_fk=group_identifier_fk, manifest_fk=None)

        file = File(**kwargs)

        data_access.update(file)

    else:  # the manifest file is present - we should have details about the file.

        # let's see if the file record has been added (with is_present == false)
        existing_record = data_access.session.query(File).filter(
            and_(File.file_name == file_name, File.group_identifier_fk == group_identifier_record.id)).first()

        if existing_record:  # record already exists - good, just update it.
            existing_record.is_present = True
            data_access.update(existing_record)
        else:  # yikes, something is broken (bad manifest file?) bug in program? You tell me.
            raise Exception("This shouldn't be. File:  " + file_name + " not listed in manifest!")


def _get_manifest_from_s3(url):
    """ Copy url from S3 to temp location
        return complete path to temp location
    """

    logger.debug(url)
    # this is a hack until we have a s3 test data set / environment
    import modules.test.data.test_data_set_two.__init__ as base
    location = base.get_location() + '/manifest.csv'
    logger.debug(location)
    return location


def _process_manifest(local_manifest_path, parsed_url, group_identifier_record):
    """ Ingest the manifest file
        get the contents of the manifest file.
        create an Upload record and persist it.
    """

    contents = get_file_contents(local_manifest_path)

    url = parsed_url.geturl()

    manifest_file = ManifestFile(group_identifier_record.location, url, contents)
    upload = Upload(manifest_file, group_identifier_record.id)
    data_access.update(upload)

    # create or update a file record for every file listed in the manifest.
    files = manifest_file.manifest_entries
    for entry in files:
        # check to see if file is already present if update it, otherwise create new record
        existing_record = data_access.session.query(File).filter(
            and_(File.file_name == entry.file_name, File.group_identifier_fk == group_identifier_record.id)).first()

        if existing_record:
            existing_record.is_present = True
            existing_record.md5_checksum = entry.md5_checksum
            existing_record.meta_data = entry.meta_data
            existing_record.size = entry.size
            existing_record.manifest_fk = upload.id

            data_access.update(existing_record)

        else:
            kwargs = dict(file_name=entry.file_name, is_present=False, file_url=entry.url,
                          md5_checksum=entry.md5_checksum,
                          meta_data=entry.meta_data,
                          size=entry.size, group_identifier_fk=upload.group_identifier_fk, manifest_fk=upload.id)

            file = File(**kwargs)
            data_access.update(file)
